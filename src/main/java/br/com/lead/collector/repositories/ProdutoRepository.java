package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
    Iterable<Produto> findAllByNomeProduto(String nomeProduto);

    Iterable<Produto> findAllByDescricaoProdutoContainingIgnoreCase(String descricaoProduto);
}
