package br.com.lead.collector.exception.errors;

import java.util.HashMap;

public class MensagemDeErro {
    private String erro;
    private String mensagemErro;
    private HashMap<String, ObjetoDeErro> camposErro;

    public MensagemDeErro(String erro) {
        this.erro = erro;
    }

    public MensagemDeErro(String erro, String mensagemErro, HashMap<String, ObjetoDeErro> camposErro) {
        this.erro = erro;
        this.mensagemErro = mensagemErro;
        this.camposErro = camposErro;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }

    public HashMap<String, ObjetoDeErro> getCamposErro() {
        return camposErro;
    }

    public void setCamposErro(HashMap<String, ObjetoDeErro> camposErro) {
        this.camposErro = camposErro;
    }
}
