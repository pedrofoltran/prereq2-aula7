package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 100, message = "Nome deve ter entre 5 à 100 caracteres")
    @NotNull(message = "Campo nome não pode ser nulo")
    @NotBlank(message = "Campo nome não pode ser vazio")
    private String nome;
    @Email(message = "Formato do e-mail invalido")
    private String email;
    @Null(message = "Campo data não deve ser preenchido")
    private LocalDate data;
    @NotNull(message = "Campo nome não pode ser vazio")
    private TipoLeadEnum tipoLead;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public Lead() {

    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }
}
