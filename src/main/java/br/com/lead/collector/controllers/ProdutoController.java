package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto criarProduto(@RequestBody Produto produto) {
        return produtoService.criarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> buscarProduto(@RequestParam(name = "nome", required = false) String nome,
                                           @RequestParam(name = "descricao", required = false) String descricao) {
        if (nome != null)
            return produtoService.buscarTodosProdutoNome(nome);
        else if (descricao != null)
            return produtoService.buscarProdutoDescricacao(descricao);
        else
            return produtoService.buscarProduto();
    }

    @GetMapping("/{id}")
    public Produto buscarProdutoID(@PathVariable int id) {
        try {
            return produtoService.buscarProdutoId(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable int id, @RequestBody Produto produto) {
        try {
            return produtoService.atualizarProduto(id, produto);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable int id) {
        try {
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/mediapreco")
    public BigDecimal mediaPrecoProduto(@RequestParam(name = "descricao", required = false) String descricao) {
        Iterable<Produto> produtoIterable;

        if (descricao == null) {
            produtoIterable = produtoService.buscarProduto();
        } else {
            produtoIterable = produtoService.buscarProdutoDescricacao(descricao);
        }
        return produtoService.calcularMedia(produtoIterable);
    }

}
