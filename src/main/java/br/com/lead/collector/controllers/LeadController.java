package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead registrarLead(@RequestBody @Valid Lead lead) {
        return leadService.salvarLead(lead);
    }

    @GetMapping
    public Iterable<Lead> exibirTodos(@RequestParam(name = "tipoDeLead", required = false) TipoLeadEnum tipoLeadEnum) {
        if (tipoLeadEnum != null)
            return leadService.buscarTodosPorTipoLead(tipoLeadEnum);
        else
            return leadService.buscarTodos();
    }

    @GetMapping("/{id}")
    public Lead buscarPorID(@PathVariable(name = "id") int id) {
        try {
            return leadService.buscarPorID(id);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable int id, @RequestBody Lead lead) {
        try {
            return leadService.atualizarLead(id, lead);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarLead(@PathVariable int id) {
        try {
            leadService.deletarLead(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
