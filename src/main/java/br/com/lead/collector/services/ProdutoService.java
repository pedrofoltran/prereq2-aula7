package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    public Produto criarProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> buscarProduto() {
        return produtoRepository.findAll();
    }

    public Iterable<Produto> buscarTodosProdutoNome(String nome) {
        return produtoRepository.findAllByNomeProduto(nome);
    }

    public Produto buscarProdutoId(int id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent())
            return optionalProduto.get();
        else
            throw new RuntimeException("O Produto nao encontrado");
    }

    public Produto atualizarProduto(int id, Produto produto) {
        if (produtoRepository.existsById(id)) {
            produto.setIdProduto(id);
            return criarProduto(produto);
        }
        throw new RuntimeException("O Produto não foi encontrado");
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("O Produto não foi encontrado");
        }
    }

    public Iterable<Produto> buscarProdutoDescricacao(String descricao) {
        Iterable<Produto> produtoIterable = produtoRepository.findAllByDescricaoProdutoContainingIgnoreCase(descricao);
        return produtoIterable;
    }

    public BigDecimal calcularMedia(Iterable<Produto> produtoIterable) {
        int quantidade = 0;
        BigDecimal soma = new BigDecimal("0");
        BigDecimal valorProduto;
        BigDecimal media;

        for (Produto produto : produtoIterable) {
            valorProduto = new BigDecimal(produto.getPrecoProduto());
            soma = soma.add(valorProduto);
            quantidade++;
        }

        if (quantidade == 0)
            media = new BigDecimal(0);
        else
            media = soma.divide(new BigDecimal(quantidade));
        return media.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }
}
