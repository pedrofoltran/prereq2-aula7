package br.com.lead.collector.service;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.services.LeadService;
import br.com.lead.collector.services.ProdutoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;
    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp() {
        lead = new Lead();
        lead.setNome("Darci");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("darci@email.com");


        produto = new Produto();
        produto.setDescricaoProduto("Cafe em po");
        produto.setIdProduto(12);
        produto.setNomeProduto("Café");
        produto.setPrecoProduto(29.99);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscarTodosOsLeads() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarSalvarLead() {

        Lead leadteste = new Lead();
        leadteste.setNome("Teste");
        leadteste.setEmail("teste@email.com");
        leadteste.setProdutos(produtos);
        leadteste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(leadteste)).thenReturn(leadteste);

        Lead leadObjeto = leadService.salvarLead(leadteste);
        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(produto, leadObjeto.getProdutos().get(0));

    }

    @Test
    public void testarBuscarTodosPorTipoLead() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAllByTipoLead(Mockito.any(TipoLeadEnum.class))).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodosPorTipoLead(TipoLeadEnum.QUENTE);
        Assertions.assertEquals(leadIterable, leads);

    }

    @Test
    public void testarBuscarPorID() {
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Lead leadObjeto = leadService.buscarPorID(12);
        Assertions.assertEquals(lead, leadObjeto);
    }


    @Test
    public void testarBuscarPorIDNaoEncontrado() {
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leadService.buscarPorID(12);
        });
    }

    @Test
    public void testarAtualizarLead() {
        Lead leadteste = new Lead();
        leadteste.setNome("Teste");
        leadteste.setEmail("teste@email.com");
        leadteste.setProdutos(produtos);
        leadteste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadteste);

        Lead leadObjeto = leadService.atualizarLead(2, leadteste);

        Assertions.assertEquals(leadteste, leadObjeto);
        Assertions.assertEquals(leadteste.getId(), leadObjeto.getId());
    }

    @Test
    public void testarAtualizarLeadNaoEncontrado() {
        Lead leadteste = new Lead();
        leadteste.setNome("Teste");
        leadteste.setEmail("teste@email.com");
        leadteste.setProdutos(produtos);
        leadteste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(false);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leadService.atualizarLead(2, leadteste);
        });
    }

    @Test
    public void testarDeleteLead(){
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);

        leadService.deletarLead(12);
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(12);
    }

    @Test
    public void testarDeleteLeadNaoEncontrado(){
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(false);

        leadService.deletarLead(12);
        Mockito.verify(leadRepository, Mockito.never()).deleteById(12);
    }
}
